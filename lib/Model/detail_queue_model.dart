class DetailQueueModel {
  DetailQueueModel({
    required this.namaMercant,
    required this.queueNumber,
    required this.nowServe,
  });

  String namaMercant;
  int queueNumber;
  dynamic nowServe;

  factory DetailQueueModel.fromJson(Map<String, dynamic> json) =>
      DetailQueueModel(
        namaMercant: json["nama_mercant"],
        queueNumber: json["queue_number"],
        nowServe: json["now_serve"],
      );

  Map<String, dynamic> toJson() => {
        "nama_mercant": namaMercant,
        "queue_number": queueNumber,
        "now_serve": nowServe,
      };
}
