class MerchantModel {
  MerchantModel({
    required this.id,
    required this.userId,
    required this.mercantName,
    required this.mercantCode,
    required this.address,
    required this.mercantEmail,
    required this.phone,
    required this.image,
    required this.createdAt,
    required this.updatedAt,
  });

  int id;
  int userId;
  String mercantName;
  String mercantCode;
  String address;
  String mercantEmail;
  String phone;
  String image;
  DateTime createdAt;
  DateTime updatedAt;

  factory MerchantModel.fromJson(Map<String, dynamic> json) => MerchantModel(
        id: json["id"],
        userId: json["user_id"],
        mercantName: json["mercant_name"],
        mercantCode: json["mercant_code"],
        address: json["address"],
        mercantEmail: json["mercant_email"],
        phone: json["phone"],
        image: json["image"],
        createdAt: DateTime.parse(json["created_at"]),
        updatedAt: DateTime.parse(json["updated_at"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "mercant_name": mercantName,
        "mercant_code": mercantCode,
        "address": address,
        "mercant_email": mercantEmail,
        "phone": phone,
        "image": image,
        "created_at": createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
      };
}
