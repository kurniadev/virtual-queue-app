class QrcodeModel {
  QrcodeModel({
    required this.id,
    required this.userId,
    required this.mercantName,
    required this.mercantEmail,
    required this.mercantCode,
    required this.address,
    required this.phone,
    required this.attachmentPath,
  });

  int id;
  int userId;
  String mercantName;
  String mercantEmail;
  String mercantCode;
  String address;
  String phone;
  String attachmentPath;

  factory QrcodeModel.fromJson(Map<String, dynamic> json) => QrcodeModel(
        id: json["id"],
        userId: json["user_id"],
        mercantName: json["mercant_name"],
        mercantEmail: json["mercant_email"],
        mercantCode: json["mercant_code"],
        address: json["address"],
        phone: json["phone"],
        attachmentPath: json["attachment_path"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "user_id": userId,
        "mercant_name": mercantName,
        "mercant_email": mercantEmail,
        "mercant_code": mercantCode,
        "address": address,
        "phone": phone,
        "attachment_path": attachmentPath,
      };
}
