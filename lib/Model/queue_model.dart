class QueueModel {
  QueueModel({
    required this.id,
    required this.mercantId,
    required this.userId,
    required this.queueNumber,
    required this.mercantName,
    required this.status,
    required this.date,
    required this.image,
    required this.address,
    required this.mercantCode,
    required this.mercantEmail,
    required this.phone,
  });

  int id;
  int mercantId;
  int userId;
  int queueNumber;
  String mercantName;
  String status;
  String date;
  String image;
  String address;
  String mercantCode;
  String mercantEmail;
  String phone;

  factory QueueModel.fromJson(Map<String, dynamic> json) => QueueModel(
        id: json["id"],
        mercantId: json["mercant_id"],
        userId: json["user_id"],
        queueNumber: json["queue_number"],
        mercantName: json["mercant_name"],
        status: json["status"],
        date: json["date"],
        image: json["image"],
        address: json["address"],
        mercantCode: json["mercant_code"],
        mercantEmail: json["mercant_email"],
        phone: json["phone"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "mercant_id": mercantId,
        "user_id": userId,
        "queue_number": queueNumber,
        "mercant_name": mercantName,
        "status": status,
        "date": date,
        "image": image,
        "address": address,
        "mercant_code": mercantCode,
        "mercant_email": mercantEmail,
        "phone": phone,
      };
}
