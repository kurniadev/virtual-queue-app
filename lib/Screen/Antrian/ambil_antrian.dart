import 'dart:convert';

import 'package:antreean_app/Model/api_response_model.dart';
import 'package:antreean_app/Model/qrcode_model.dart';
import 'package:antreean_app/Screen/Antrian/berhasil_add_queue.dart';
import 'package:antreean_app/Screen/onboarding.dart';
import 'package:antreean_app/Screen/tabsscreen.dart';
import 'package:antreean_app/Service/auth_service.dart';
import 'package:antreean_app/Service/qrcode_service.dart';
import 'package:antreean_app/Service/queue_service.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:lottie/lottie.dart';
import 'package:skeletons/skeletons.dart';

import '../../constan.dart';

class AmbilAntrian extends StatefulWidget {
  const AmbilAntrian({Key? key, required this.codeQr}) : super(key: key);

  final String codeQr;

  @override
  State<AmbilAntrian> createState() => _AmbilAntrianState();
}

class _AmbilAntrianState extends State<AmbilAntrian> {
  QrcodeModel? qrcodeModel;
  bool loading = true;
  String _someting = "";

  void _getQrcode() async {
    ApiResponse response = await getQrCode(widget.codeQr);
    if (response.error == null) {
      setState(() {
        qrcodeModel = response.data as QrcodeModel;
        loading = false;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => {
            Navigator.of(context).pushAndRemoveUntil(
                MaterialPageRoute(builder: (context) => Onboarding()),
                (route) => false)
          });
    }
    // else if (widget.codeQr == '-1') {
    //   Navigator.of(context).pushAndRemoveUntil(
    //       MaterialPageRoute(builder: (context) => Tabsscreen()),
    //       (route) => false);
    // }
    else {
      setState(() {
        String? sometingku = response.error;
        _someting = sometingku!;
        loading = false;
      });
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Merchant not available')));
    }
  }

  // void _addQueue() async {
  //   ApiResponse response = await AddQueueService();
  //   if (response.error == null) {
  //     Navigator.of(context).pushAndRemoveUntil(
  //         MaterialPageRoute(builder: (context) => BerhasilAdd()),
  //         (route) => false);
  //   } else if (response.error == unauthorized) {
  //     logout().then((value) => {
  //           Navigator.of(context).pushAndRemoveUntil(
  //               MaterialPageRoute(builder: (context) => Onboarding()),
  //               (route) => false)
  //         });
  //   } else {
  //     print("object Account");
  //     ScaffoldMessenger.of(context)
  //         .showSnackBar(SnackBar(content: Text('${response.error}')));
  //   }
  // }

  // void AddQueueku() async {
  //   String token = await getToken();
  //   int userId = await getUserId();
  //   print(qrcodeModel!.id);
  //   print(userId);
  //   final responsesku = await http.post(
  //       Uri.parse("http://antree-website.herokuapp.com/api/queue"),
  //       headers: {
  //         'Accept': 'application/json',
  //         'X-Requested-With': 'XMLHttpRequest',
  //         'Authorization': 'Bearer $token'
  //       },
  //       body: {
  //         // 'mercant_id': qrcodeModel!.id,
  //         'user_id': userId,
  //       });

  //   final dataku = jsonDecode(responsesku.body);
  //   int codeku = dataku['meta']['code'];
  //   // String EmailError = data['data']['errors'];
  //   print(codeku);

  //   if (codeku == 200) {
  //     Navigator.pop(context);
  //     Navigator.of(context).pushAndRemoveUntil(
  //         MaterialPageRoute(builder: (context) => BerhasilAdd()),
  //         (route) => false);
  //   } else {
  //     ScaffoldMessenger.of(context)
  //         .showSnackBar(SnackBar(content: Text('Failed')));
  //     Navigator.pop(context);
  //   }
  // }

  showAlertDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(
            color: Colors.blueAccent,
          ),
          Container(margin: EdgeInsets.only(left: 5), child: Text("Loading")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void AddQueue() async {
    showAlertDialog(context);
    int idUser = await getUserId();
    String token = await getToken();
    final response =
        await http.post(Uri.parse(baseURL + '/api/queue'), headers: {
      'Accept': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      'Authorization': 'Bearer $token'
    }, body: {
      'user_id': idUser.toString(),
      'mercant_id': qrcodeModel!.id.toString(),
    });

    final data = jsonDecode(response.body);
    int code = data['meta']['code'];
    // String EmailError = data['data']['errors'];
    print(code);

    if (code == 200) {
      Navigator.pop(context);
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => BerhasilAdd()),
          (route) => false);
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Failed')));
      Navigator.pop(context);
    }
  }

  @override
  void initState() {
    _getQrcode();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0XFFE1F5FE),
        appBar: AppBar(
          elevation: 0,
          title: Text("Submit Queue",
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.black)),
          backgroundColor: Colors.white,
        ),
        body: Skeleton(
          isLoading: loading,
          skeleton: Container(
            alignment: Alignment.center,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  child: Lottie.asset(
                    "assets/lottie/loadingku.json",
                  ),
                )
              ],
            ),
          ),
          child: _someting == "somethingWhentWrong"
              ? Container(
                  child: Column(
                    children: <Widget>[
                      Lottie.asset(
                        "assets/lottie/something.json",
                      ),
                      Text(
                        "Merchant not available",
                        style: TextStyle(fontWeight: FontWeight.w400),
                      )
                    ],
                  ),
                )
              : Container(
                  height: MediaQuery.of(context).size.height,
                  width: double.infinity,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Expanded(
                          child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              Text(
                                "${qrcodeModel?.mercantName}",
                                style: TextStyle(
                                    fontSize: 30, fontWeight: FontWeight.bold),
                              ),
                              SizedBox(
                                height: 20,
                              ),
                              Text(
                                "Do you want to take the queue here?",
                                style: TextStyle(
                                    fontSize: 15, color: Colors.grey[700]),
                              )
                            ],
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 40),
                            child: Column(
                              children: <Widget>[
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "Code Merchant",
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.black87),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Container(
                                      height: 40,
                                      width: MediaQuery.of(context).size.width,
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Text(
                                          "${qrcodeModel?.mercantCode}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    )
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "Address",
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.black87),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Container(
                                      height: 40,
                                      width: MediaQuery.of(context).size.width,
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Text(
                                          "${qrcodeModel?.address}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    )
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "Phone",
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.black87),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Container(
                                      height: 40,
                                      width: MediaQuery.of(context).size.width,
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Text(
                                          "${qrcodeModel?.phone}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    )
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      "Email",
                                      style: TextStyle(
                                          fontSize: 15,
                                          fontWeight: FontWeight.w400,
                                          color: Colors.black87),
                                    ),
                                    SizedBox(
                                      height: 5,
                                    ),
                                    Container(
                                      height: 40,
                                      width: MediaQuery.of(context).size.width,
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius:
                                              BorderRadius.circular(10)),
                                      child: Padding(
                                        padding: const EdgeInsets.all(10.0),
                                        child: Text(
                                          "${qrcodeModel?.mercantEmail}",
                                          style: TextStyle(
                                              fontWeight: FontWeight.w400),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 10,
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 40),
                            child: Container(
                              padding: EdgeInsets.only(top: 3, left: 3),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                              ),
                              child: MaterialButton(
                                minWidth: double.infinity,
                                height: 60,
                                onPressed: () {
                                  AddQueue();
                                },
                                color: Color(0xff0095FF),
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50),
                                ),
                                child: Text(
                                  "Take The Queue",
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 18,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 40),
                            child: Container(
                              padding: EdgeInsets.only(left: 3),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                              ),
                              child: MaterialButton(
                                minWidth: double.infinity,
                                height: 60,
                                onPressed: () {
                                  Navigator.of(context).pushAndRemoveUntil(
                                      MaterialPageRoute(
                                          builder: (context) => Tabsscreen()),
                                      (route) => false);
                                },
                                color: Colors.redAccent,
                                elevation: 0,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(50),
                                ),
                                child: Text(
                                  "Cencel",
                                  style: TextStyle(
                                    fontWeight: FontWeight.w600,
                                    fontSize: 18,
                                    color: Colors.white,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ))
                    ],
                  ),
                ),
        ));
  }
}
