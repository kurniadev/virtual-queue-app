import 'dart:async';

import 'package:antreean_app/Screen/Homepage/antrian.dart';
import 'package:antreean_app/Screen/tabsscreen.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class BerhasilAdd extends StatefulWidget {
  const BerhasilAdd({Key? key}) : super(key: key);

  @override
  State<BerhasilAdd> createState() => _BerhasilAddState();
}

class _BerhasilAddState extends State<BerhasilAdd> {
  @override
  Widget build(BuildContext context) {
    startApp(context);
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: Lottie.asset(
                "assets/lottie/success.json",
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "You managed to take the queue",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
            )
          ],
        ),
      ),
    );
  }

  startApp(BuildContext context) {
    Timer(Duration(seconds: 3), () {
      Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (_) {
        return Tabsscreen();
      }));
    });
  }
}
