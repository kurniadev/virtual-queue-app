import 'package:antreean_app/Model/api_response_model.dart';
import 'package:antreean_app/Model/queue_model.dart';
import 'package:antreean_app/Screen/Detail/detail_antrian.dart';
import 'package:antreean_app/Screen/onboarding.dart';
import 'package:antreean_app/Service/auth_service.dart';
import 'package:antreean_app/Service/queue_service.dart';
import 'package:antreean_app/constan.dart';
import 'package:antreean_app/widget/list_antrian.dart';
import 'package:flutter/material.dart';
import 'package:skeletons/skeletons.dart';

class AntrianScreen extends StatefulWidget {
  const AntrianScreen({Key? key}) : super(key: key);

  @override
  State<AntrianScreen> createState() => _AntrianScreenState();
}

class _AntrianScreenState extends State<AntrianScreen> {
  List<dynamic> _QueueList = [];
  int userId = 0;
  bool _loading = true;

  Future<void> retrieveQueue() async {
    userId = await getUserId();
    print(userId);
    ApiResponse response = await getQueue();
    if (response.error == null) {
      setState(() {
        print(_QueueList);
        _QueueList = response.data as List<dynamic>;
        _loading = _loading ? !_loading : _loading;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => Onboarding()),
          (route) => false));
    } else {
      print("object history");
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('${response.error}')));
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    retrieveQueue();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0XFFE1F5FE),
        appBar: AppBar(
          elevation: 0,
          title: Text("Queue",
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.black)),
          backgroundColor: Colors.white,
        ),
        body: Skeleton(
          isLoading: _loading,
          skeleton: ListView(
            children: <Widget>[
              Container(
                child: Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 90,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        height: 90,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        height: 90,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        height: 90,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          child: RefreshIndicator(
            onRefresh: () {
              return retrieveQueue();
            },
            child: ListView.builder(
              itemCount: _QueueList.length,
              itemBuilder: (BuildContext context, int index) {
                QueueModel queueModel = _QueueList[index];
                return InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => DetailAntrian(
                                  queueModel: queueModel,
                                )));
                  },
                  child: Container(
                      child: Padding(
                    padding:
                        const EdgeInsets.only(left: 20, right: 20, top: 20),
                    child: Column(
                      children: <Widget>[
                        Container(
                          height: 90,
                          width: MediaQuery.of(context).size.width,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.white),
                          child: Row(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.only(left: 10),
                                child: Container(
                                  height: 60,
                                  width: 60,
                                  child: ClipRRect(
                                    child: Image.asset(
                                      'assets/image/profile.png',
                                      fit: BoxFit.cover,
                                    ),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 10),
                                child: Container(
                                    width:
                                        MediaQuery.of(context).size.width * 0.7,
                                    height: 60,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          "${queueModel.mercantName}",
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                        Text("${queueModel.address}",
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis),
                                        const SizedBox(
                                          height: 5,
                                        ),
                                        queueModel.status == 'pending'
                                            ? Container(
                                                height: 20,
                                                width: 75,
                                                decoration: BoxDecoration(
                                                    color: Colors.redAccent,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10)),
                                                child: Center(
                                                  child: Text(
                                                    "${queueModel.status}",
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                    ),
                                                  ),
                                                ),
                                              )
                                            : Container(
                                                height: 20,
                                                width: 75,
                                                decoration: BoxDecoration(
                                                    color: Colors.indigoAccent,
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            10)),
                                                child: Center(
                                                  child: Text(
                                                    "${queueModel.status}",
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                    ),
                                                  ),
                                                ),
                                              )
                                      ],
                                    )),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  )),
                );
              },
            ),
          ),
        ));
  }
}
