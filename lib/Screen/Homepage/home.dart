import 'package:antreean_app/Screen/Antrian/ambil_antrian.dart';
import 'package:antreean_app/Screen/Detail/detail_antrian.dart';
import 'package:antreean_app/Screen/tabsscreen.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  // String code = "";
  // String getCode = "";

  Future scanbarcode() async {
    await FlutterBarcodeScanner.scanBarcode(
            "#00BFFF", "CANCEL", true, ScanMode.DEFAULT)
        .then((String code) {
      // ignore: unnecessary_null_comparison
      if (code == '-1') {
        Navigator.of(context).pushAndRemoveUntil(
            MaterialPageRoute(builder: (context) => Tabsscreen()),
            (route) => false);
      } else {
        Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => AmbilAntrian(
                      codeQr: code,
                    )));
      }
    });
  }

  String name = "";

  getPref() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    setState(() {
      name = preferences.getString("name")!;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getPref();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0XFFE1F5FE),
      body: ListView(
        children: <Widget>[
          Container(
            padding: const EdgeInsets.only(top: 30, left: 20, right: 20),
            height: 120,
            width: double.infinity,
            decoration: const BoxDecoration(
              borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(20),
                bottomRight: Radius.circular(20),
              ),
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                stops: [0.1, 0.5],
                colors: [
                  Color(0xFF0288D1),
                  Colors.blue,
                ],
              ),
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Hello,\n$name",
                      style: TextStyle(
                        fontWeight: FontWeight.w600,
                        fontSize: 18.0,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
          SizedBox(
            height: 30,
          ),
          Container(
            height: MediaQuery.of(context).size.height / 2,
            child: Lottie.asset("assets/lottie/qrcode.json"),
          ),
          Padding(
            padding: EdgeInsets.only(left: 10, right: 10),
            child: Container(
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Color(0xFF29B6F6),
              ),
              height: 60,
              width: MediaQuery.of(context).size.width,
              child: Row(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width * 0.3,
                    height: 60,
                    child: Icon(
                      Icons.qr_code,
                      size: 40,
                      color: Colors.white,
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      scanbarcode();
                    },
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.5,
                      height: 20,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "Scan Barcodes",
                            style: TextStyle(
                              fontWeight: FontWeight.w600,
                              fontSize: 18.0,
                              color: Colors.white,
                            ),
                          ),
                          Icon(
                            Icons.arrow_right_alt,
                            color: Colors.white,
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
