import 'dart:convert';

import 'package:antreean_app/Model/api_response_model.dart';
import 'package:antreean_app/Model/merchant_model.dart';
import 'package:antreean_app/Screen/Antrian/berhasil_add_queue.dart';
import 'package:antreean_app/Screen/onboarding.dart';
import 'package:antreean_app/Screen/tabsscreen.dart';
import 'package:antreean_app/Service/auth_service.dart';
import 'package:antreean_app/Service/merchant_service.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:skeletons/skeletons.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import '../../constan.dart';

class MerchantScreen extends StatefulWidget {
  const MerchantScreen({Key? key}) : super(key: key);

  @override
  State<MerchantScreen> createState() => _MerchantScreenState();
}

class _MerchantScreenState extends State<MerchantScreen> {
  List<dynamic> _MerchantList = [];
  int userId = 0;
  bool _loading = true;

  //get all history
  Future<void> retrieveMerchant() async {
    userId = await getUserId();
    print(userId);
    ApiResponse response = await getMerchant();
    if (response.error == null) {
      setState(() {
        print(_MerchantList);
        _MerchantList = response.data as List<dynamic>;
        _loading = _loading ? !_loading : _loading;
      });
    } else if (response.error == unauthorized) {
      logout().then((value) => Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => Onboarding()),
          (route) => false));
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('${response.error}')));
    }
  }

  showAlertDialog(BuildContext context) {
    AlertDialog alert = AlertDialog(
      content: new Row(
        children: [
          CircularProgressIndicator(
            color: Colors.blueAccent,
          ),
          Container(margin: EdgeInsets.only(left: 5), child: Text("Loading")),
        ],
      ),
    );
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  void AddQueue(String merchantId) async {
    showAlertDialog(context);
    int idUser = await getUserId();
    String token = await getToken();
    final response =
        await http.post(Uri.parse(baseURL + '/api/queue'), headers: {
      'Accept': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      'Authorization': 'Bearer $token'
    }, body: {
      'user_id': idUser.toString(),
      'mercant_id': merchantId,
    });

    final data = jsonDecode(response.body);
    int code = data['meta']['code'];
    // String EmailError = data['data']['errors'];
    print(code);

    if (code == 200) {
      Navigator.pop(context);
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => BerhasilAdd()),
          (route) => false);
    } else if (code == 400) {
      Navigator.pop(context);
      Alert(
        context: context,
        type: AlertType.info,
        title: "Antree App",
        desc: "Out of Quota",
        buttons: [
          DialogButton(
            child: Text(
              "Back",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
            onPressed: () => Navigator.pop(context),
            width: 120,
          )
        ],
      ).show();
    } else {
      ScaffoldMessenger.of(context)
          .showSnackBar(SnackBar(content: Text('Failed')));
      Navigator.pop(context);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    retrieveMerchant();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0XFFE1F5FE),
      appBar: AppBar(
        elevation: 0,
        title: Text(
          "Merchant",
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
      ),
      body: Skeleton(
        isLoading: _loading,
        skeleton: ListView(
          children: <Widget>[
            Container(
              child: Padding(
                padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 90,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.white),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      height: 90,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.white),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      height: 90,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.white),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Container(
                      height: 90,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.white),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        child: RefreshIndicator(
          onRefresh: () {
            return retrieveMerchant();
          },
          child: ListView.builder(
            itemCount: _MerchantList.length,
            itemBuilder: (BuildContext context, int index) {
              MerchantModel merhchantModel = _MerchantList[index];
              print(merhchantModel);
              return InkWell(
                onTap: () {
                  showDialog(
                      context: context,
                      builder: (context) {
                        return Dialog(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(
                                  20.0)), //this right here
                          child: Container(
                            height: 400,
                            child: Padding(
                              padding: const EdgeInsets.all(15.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: <Widget>[
                                      InkWell(
                                        onTap: () {
                                          Navigator.pop(context);
                                        },
                                        child: Container(
                                          alignment: Alignment.topRight,
                                          width: 35,
                                          height: 35,
                                          decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                              color: Colors.red),
                                          child: Center(
                                            child: Text(
                                              'X',
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                  Text(
                                    "${merhchantModel.mercantName}",
                                    maxLines: 2,
                                    overflow: TextOverflow.ellipsis,
                                    style: TextStyle(
                                      fontSize: 28,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 25,
                                  ),
                                  Text(
                                    "Address",
                                    style:
                                        TextStyle(fontWeight: FontWeight.w700),
                                  ),
                                  Text("${merhchantModel.address}",
                                      maxLines: 3,
                                      overflow: TextOverflow.ellipsis),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "Phone",
                                    style:
                                        TextStyle(fontWeight: FontWeight.w700),
                                  ),
                                  Text("${merhchantModel.phone}",
                                      maxLines: 3,
                                      overflow: TextOverflow.ellipsis),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "Email",
                                    style:
                                        TextStyle(fontWeight: FontWeight.w700),
                                  ),
                                  Text("${merhchantModel.mercantEmail}",
                                      maxLines: 3,
                                      overflow: TextOverflow.ellipsis),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Text(
                                    "Code",
                                    style:
                                        TextStyle(fontWeight: FontWeight.w700),
                                  ),
                                  Text("${merhchantModel.mercantCode}",
                                      maxLines: 3,
                                      overflow: TextOverflow.ellipsis),
                                  SizedBox(
                                    height: 40,
                                  ),
                                  Center(
                                    child: SizedBox(
                                      width: 200,
                                      child: RaisedButton(
                                        onPressed: () {
                                          showDialog(
                                              context: context,
                                              builder: (context) {
                                                return Container(
                                                  child: AlertDialog(
                                                    title: Text(
                                                        "Do you want to Take Queue?"),
                                                    actions: [
                                                      TextButton(
                                                          onPressed: () {
                                                            Navigator.pop(
                                                                context);
                                                          },
                                                          child:
                                                              Text("Cencel")),
                                                      TextButton(
                                                        onPressed: () {
                                                          AddQueue(
                                                              merhchantModel.id
                                                                  .toString());
                                                        },
                                                        child: Text(
                                                          "Yes",
                                                          style: TextStyle(
                                                              color: Colors
                                                                  .redAccent),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                );
                                              });
                                        },
                                        child: Text(
                                          "Take The Queue",
                                          style: TextStyle(color: Colors.white),
                                        ),
                                        color: Colors.lightGreen,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      });
                },
                child: Container(
                    child: Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20, top: 20),
                  child: Column(
                    children: <Widget>[
                      Container(
                        height: 90,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white),
                        child: Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Container(
                                height: 60,
                                width: 60,
                                child: ClipRRect(
                                  child: Image.asset(
                                    'assets/image/profile.png',
                                    fit: BoxFit.cover,
                                  ),
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 10,
                              ),
                              child: Container(
                                  width:
                                      MediaQuery.of(context).size.width * 0.7,
                                  height: 60,
                                  child: Padding(
                                    padding: const EdgeInsets.only(right: 10),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        Text(
                                          "${merhchantModel.mercantName}",
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                        Text("${merhchantModel.address}",
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis),
                                        SizedBox(
                                          height: 5,
                                        ),
                                      ],
                                    ),
                                  )),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                )),
              );
            },
          ),
        ),
      ),
    );
  }
}
