import 'package:antreean_app/Screen/Homepage/account.dart';
import 'package:antreean_app/Screen/Homepage/antrian.dart';
import 'package:antreean_app/Screen/Homepage/history.dart';
import 'package:antreean_app/Screen/Homepage/home.dart';
import 'package:antreean_app/Screen/Homepage/merchant.dart';
import 'package:flutter/material.dart';

class Tabsscreen extends StatefulWidget {
  const Tabsscreen({Key? key}) : super(key: key);

  @override
  State<Tabsscreen> createState() => _TabsscreenState();
}

class _TabsscreenState extends State<Tabsscreen> {
  int _currentIndex = 0;

  final tabs = [
    const HomeScreen(),
    const AntrianScreen(),
    const MerchantScreen(),
    const HistoryScreen(),
    const AccountScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: tabs[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: _currentIndex,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text("Home"),
              backgroundColor: Color(0xff0095FF),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.qr_code),
              title: Text("Queue"),
              backgroundColor: Color(0xff0095FF),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.store),
              title: Text("Merchant"),
              backgroundColor: Color(0xff0095FF),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.history),
              title: Text("History"),
              backgroundColor: Color(0xff0095FF),
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.person),
              title: Text("Account"),
              backgroundColor: Color(0xff0095FF),
            ),
          ],
          onTap: (index) {
            setState(() {
              _currentIndex = index;
            });
          }),
    );
  }
}
