import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class Testingku extends StatefulWidget {
  const Testingku({Key? key}) : super(key: key);

  @override
  State<Testingku> createState() => _TestingkuState();
}

class _TestingkuState extends State<Testingku> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: Lottie.asset(
                "assets/lottie/success.json",
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Text(
              "You managed to take the queue",
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
            )
          ],
        ),
      ),
    );
  }
}
