import 'dart:convert';

import 'package:antreean_app/Model/api_response_model.dart';
import 'package:antreean_app/Model/merchant_model.dart';
import 'package:http/http.dart' as http;

import '../constan.dart';
import 'auth_service.dart';

//Get Merchant
String apiMerchant = baseURL + '/api/mercant';

Future<ApiResponse> getMerchant() async {
  ApiResponse apiResponse = ApiResponse();
  try {
    String token = await getToken();
    final response = await http.get(Uri.parse(apiMerchant), headers: {
      'Accept': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      'Authorization': 'Bearer $token'
    });

    switch (response.statusCode) {
      case 200:

        // print(response.body);
        apiResponse.data = jsonDecode(response.body)['data'][0]
            .map((p) => MerchantModel.fromJson(p))
            .toList();
        apiResponse.data as List<dynamic>;
        break;
      case 401:
        apiResponse.error = unauthorized;
        break;
      default:
        apiResponse.error = somethingWhentWrong;
        break;
    }
  } catch (e) {
    apiResponse.error = serverError;
  }
  return apiResponse;
}
