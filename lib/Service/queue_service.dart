import 'dart:convert';

import 'package:antreean_app/Model/api_response_model.dart';
import 'package:antreean_app/Model/detail_queue_model.dart';
import 'package:antreean_app/Model/queue_model.dart';
import 'package:antreean_app/Service/auth_service.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

import '../constan.dart';

//Get Queue
String apiQueue = baseURL + '/api/queue/get';

Future<ApiResponse> getQueue() async {
  ApiResponse apiResponse = ApiResponse();
  try {
    String token = await getToken();
    final response = await http.get(Uri.parse(apiQueue), headers: {
      'Accept': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      'Authorization': 'Bearer $token'
    });

    switch (response.statusCode) {
      case 200:
        print(response.body);
        apiResponse.data = jsonDecode(response.body)['data'][0]
            .map((p) => QueueModel.fromJson(p))
            .toList();
        apiResponse.data as List<dynamic>;
        break;
      case 401:
        apiResponse.error = unauthorized;
        break;
      default:
        apiResponse.error = somethingWhentWrong;
        break;
    }
  } catch (e) {
    apiResponse.error = serverError;
  }
  return apiResponse;
}

//Get Detail Queue
String apiQueueDetail = baseURL + '/api/queue/get/';

Future<ApiResponse> getQueueDetail(String Id) async {
  ApiResponse apiResponse = ApiResponse();
  try {
    String token = await getToken();
    final response = await http.get(Uri.parse(apiQueueDetail + Id), headers: {
      'Accept': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      'Authorization': 'Bearer $token'
    });

    switch (response.statusCode) {
      case 200:
        print(response.body);
        print("Test Respon");
        apiResponse.data =
            DetailQueueModel.fromJson(jsonDecode(response.body)['data']);
        break;
      case 401:
        apiResponse.error = unauthorized;
        break;
      default:
        apiResponse.error = somethingWhentWrong;

        break;
    }
  } catch (e) {
    apiResponse.error = serverError;
  }
  return apiResponse;
}

//Add Queue

String apiAddQueue = baseURL + '/api/queue';

Future<ApiResponse> AddQueueService() async {
  ApiResponse apiResponse = ApiResponse();
  try {
    String token = await getToken();
    int idUser = await getUserId();
    final response = await http.post(Uri.parse(apiAddQueue), headers: {
      'Accept': 'application/json',
      'Authorization': 'Bearer $token'
    }, body: {
      'user_id': idUser,
      // 'mercant_id': mercant_id,
    });
    switch (response.statusCode) {
      case 200:
        apiResponse.data = jsonDecode(response.body)['message'];
        break;
      case 401:
        apiResponse.error = unauthorized;
        break;
      default:
        apiResponse.error = somethingWhentWrong;
        break;
    }
  } catch (e) {
    apiResponse.error = serverError;
  }
  return apiResponse;
}
