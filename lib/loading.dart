import 'package:antreean_app/Model/api_response_model.dart';
import 'package:antreean_app/Screen/onboarding.dart';
import 'package:antreean_app/Screen/tabsscreen.dart';
import 'package:antreean_app/Service/auth_service.dart';
import 'package:antreean_app/constan.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoadingAwal extends StatefulWidget {
  const LoadingAwal({Key? key}) : super(key: key);

  @override
  State<LoadingAwal> createState() => _LoadingAwalState();
}

class _LoadingAwalState extends State<LoadingAwal> {
  void _loadUserInfo() async {
    String token = await getToken();
    if (token == '') {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => Onboarding()),
          (route) => false);
    } else {
      Navigator.of(context).pushAndRemoveUntil(
          MaterialPageRoute(builder: (context) => Tabsscreen()),
          (route) => false);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    _loadUserInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: CircularProgressIndicator(
        color: Colors.redAccent,
      ),
    ));
  }
}
