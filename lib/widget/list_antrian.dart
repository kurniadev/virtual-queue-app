import 'package:antreean_app/Screen/Detail/detail_antrian.dart';
import 'package:flutter/material.dart';

class ListAntrian extends StatefulWidget {
  const ListAntrian({Key? key}) : super(key: key);

  @override
  State<ListAntrian> createState() => _ListAntrianState();
}

class _ListAntrianState extends State<ListAntrian> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 90,
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(10), color: Colors.white),
      child: Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Container(
              height: 60,
              width: 60,
              child: ClipRRect(
                child: Image.network(
                  "https://1.bp.blogspot.com/-WyymufOIBp8/XA4lDhcykbI/AAAAAAAADTw/11sG4ujL324Uk9IB0ZKsBy19ZrRmLxQBACLcBGAs/s1600/Logo%2B2.jpg",
                  fit: BoxFit.cover,
                ),
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
          SizedBox(
            width: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Container(
                width: MediaQuery.of(context).size.width * 0.7,
                height: 60,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    const Text(
                      "Kedai Ma Ning Happines",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    const Text(
                        "Jl. Lidah Wetan, Lidah Wetan, Kec. Lakarsantri, Kota SBY, Jawa Timur 60213",
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis),
                    const SizedBox(
                      height: 5,
                    ),
                    Container(
                      height: 20,
                      width: 75,
                      decoration: BoxDecoration(
                          color: Colors.lightBlueAccent,
                          borderRadius: BorderRadius.circular(10)),
                      child: Center(child: Text("Ongoing")),
                    )
                  ],
                )),
          )
        ],
      ),
    );
  }
}
